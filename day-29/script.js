// get element for the Table Of Content
var toc = document.querySelector('#table-of-contents');
// get all h2 Headings
var headings = document.querySelectorAll('h2');
       
// build HTML for a single Heading
var buildHeadingLink = function (heading) {
    return '<li>' +
			'<a href=\"#'+heading.id+'\">' + heading.textContent + '</a>' +
            '</li>';
}
// if any headings are found, build Table-Of-Content and inject in toc Element
if (headings.length > 0) {
	toc.innerHTML = '<ol>' +
    	Array.prototype.slice.call(headings)
		.map(buildHeadingLink)
 		.join('') +
	'</ol>';
}
