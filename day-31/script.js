// get element for the Table Of Content
var toc = document.querySelector('#table-of-contents');
// get all h2 Headings
var headings = document.querySelectorAll('h2');
       
// build HTML for a single Heading
var buildHeadingLink = function (heading, index) {
  //if(heading.id === ''){  // https://developer.mozilla.org/en-US/docs/Glossary/Falsy
    if(!heading.id) {
      heading.id = heading.textContent
        .replace(/\W+/g,'-') // thanks to Kieran "RegEx-God" Barker 
        .toLowerCase();
      }
  /**
  // 1st try: replace space and single quotation mark with dash globaly
  .replace(new RegExp(' |\'','g'),' ')
  // 2nd try to avoid the double dash --: replace single quotation mark with space, then replace space
  .replace(/'/g,' ').replace(/\s+/g,'-')
  // thanks to Kieran "RegEx-God" Barker: the "non Word" \W RegExp:
  .replace(/\W+/g,'-')
  **/
    return '<li>' +
			  '<a href=\"#'+heading.id+'\">' +
            heading.textContent +
        '</a>' +
      '</li>';
}
// if any headings are found, build Table-Of-Content and inject in toc Element
if (headings.length > 0) {
	toc.innerHTML = '<ol>' +
    Array.prototype.slice.call(headings)
		.map(buildHeadingLink)
 		.join('') +
	'</ol>';
}
