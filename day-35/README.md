# Vanilla JS Academy » Day 35

## Project: Weather App

**Task:**

Build an app that gets a user’s location and displays their current weather information.


**Demo:**

https://mayoneese.gitlab.io/vanilla-js-academy/day-35/index.html