// wrapping everything inside a IIEF (Immediately Invoked Function Expression)
// to keep everything out of the global scope.
;(function () {

  // get element of app
  var app = document.querySelector('#app');
  // endpoint of ipapi
  var ep_ipapi = 'https://ipapi.co/json';
  // endpoint and key of weatherbit.io
  var key_weatherbit = '91a4affdade6413fb8f5423042acf59e';
  var ep_weatherbit = 'https://api.weatherbit.io/v2.0/current?key=' + key_weatherbit;
  /*!
   * Sanitize and encode all HTML in a user-submitted string
   * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
   * @param  {String} str  The user-submitted string
   * @return {String} str  The sanitized string
  */
  var sanitizeHTML = function (str) {
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
  };
  
  // add leading Zero
  var addLeadingZero = function (part) {
    return (part.length < 2) ? '0' + part : part;
  }
  // Format the date string
  var formatDate = function (date) {
      var d = new Date(date),
          month = addLeadingZero('' + (d.getMonth() + 1)),
          day = addLeadingZero('' + d.getDate()),
          year = d.getFullYear(),
          hour = addLeadingZero('' + (d.getHours() + 1)),
          minute = addLeadingZero('' + (d.getMinutes() + 1));

      //return [month, day, year].join('/') + ' ' + [hour, minute].join(':');
      return [day, month, year].join('.') + ' ' + [hour, minute].join(':');
  }

  // Celsius 2 Fahrenheit
  var cel2far = function (temp_c) { return (temp_c * 9 / 5) + 32; }
  // Fahrenheit 2 Celsius
  var far2cel = function (temp_f) { return (temp - 32) * 5 / 9; }
  
  // return JSON if request was ok
  var getJSON = function(response){
    return response.ok ? response.json() : Promise.reject(response);
  }
  
  // errorhandling
  var handleError = function (error) {
    console.warn('Something went wrong.', error);
    app.innerHTML = '<div>' +
       '<h2>Oops...</h2>' +
       '<p>' +
        'Something went wrong when trying to catch the weather near you. Please try again later...'+
      '</p>' +
    '</div>';     
  }
  
  // get location from ipapi data
  var getLocation = function (data) {
    return { longitude: sanitizeHTML(data.longitude), latitude: sanitizeHTML(data.latitude) };
  }
  
  // get weather from location
  var getWeather = function (location) {
    return fetch(ep_weatherbit + '&lat=' + location.latitude + '&lon=' + location.longitude); 
  }
  // get weather data
  var getWeatherData = function (data) {
    if (data.data.length > 0) {
        let d = data.data[0];
        console.log(d);
        app.innerHTML = '<div>'+
          '<h2>'+sanitizeHTML(d.city_name)+'</h2>'+
          '<div><span class=\"tempc\">'+sanitizeHTML(d.temp)+'°C</span><span class=\"tempf\">('+cel2far(sanitizeHTML(d.temp))+'°F)</span></div>'+
          '<div>'+
            '<img height=\"150\" width=\"150\" alt=\"' + sanitizeHTML(d.weather.description) + '\" src="https://weatherbit.io/static/img/icons/' + sanitizeHTML(d.weather.icon) + '.png\">'+
          '<div class=\"desc\">' + sanitizeHTML(d.weather.description) + '</div>'+
          '<div class=\"lastob\">Last observed: ' + formatDate(sanitizeHTML(d.last_ob_time)) + '</div>'+
          '</div>'+
        '</div>' 
      }
  }
  // fetch weather 
  var fetchWeather = function (){
    console.log("fetch weather...");
    return fetch(ep_ipapi)
    .then(getJSON)
    .then(getLocation)
    .then(getWeather)
    .then(getJSON)
    .then(getWeatherData)
    .catch(handleError);
  }
  fetchWeather();
  
})();
